-- --------------------------------------------------------
-- Host:                         localhost
-- Versión del servidor:         5.7.24 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para kinesiologiadb
CREATE DATABASE IF NOT EXISTS `kinesiologiadb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `kinesiologiadb`;

-- Volcando estructura para tabla kinesiologiadb.alumnos
CREATE TABLE IF NOT EXISTS `alumnos` (
  `id_alumno` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_alumno` varchar(60) DEFAULT NULL,
  `apellido_alumno` varchar(60) DEFAULT NULL,
  `telefono_alumno` varchar(60) DEFAULT NULL,
  `correo_alumno` varchar(60) DEFAULT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_alumno`),
  KEY `fk_Alumnos_Usuarios1_idx` (`id_usuario`),
  CONSTRAINT `fk_Alumnos_Usuarios1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla kinesiologiadb.alumnos: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `alumnos` DISABLE KEYS */;
INSERT INTO `alumnos` (`id_alumno`, `nombre_alumno`, `apellido_alumno`, `telefono_alumno`, `correo_alumno`, `id_usuario`) VALUES
	(1, 'Juan ', 'Pueblo', '123456', 'yo@tu.el', 1),
	(2, 'Juan ', 'Perez', '123456', 'yo@tu.el', 1);
/*!40000 ALTER TABLE `alumnos` ENABLE KEYS */;

-- Volcando estructura para tabla kinesiologiadb.antecedentes
CREATE TABLE IF NOT EXISTS `antecedentes` (
  `id_antecedente` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion_antecedente` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_antecedente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla kinesiologiadb.antecedentes: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `antecedentes` DISABLE KEYS */;
/*!40000 ALTER TABLE `antecedentes` ENABLE KEYS */;

-- Volcando estructura para tabla kinesiologiadb.antecedentes_derivaciones
CREATE TABLE IF NOT EXISTS `antecedentes_derivaciones` (
  `id_antecedente` int(11) NOT NULL,
  `id_derivacion` int(11) NOT NULL,
  PRIMARY KEY (`id_antecedente`,`id_derivacion`),
  KEY `fk_Antecedentes_de_Derivacion_Antecedentes1_idx` (`id_antecedente`),
  KEY `fk_Antecedentes_de_Derivacion_Derivacion1_idx` (`id_derivacion`),
  CONSTRAINT `fk_Antecedentes_de_Derivacion_Antecedentes1` FOREIGN KEY (`id_antecedente`) REFERENCES `antecedentes` (`id_antecedente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Antecedentes_de_Derivacion_Derivacion1` FOREIGN KEY (`id_derivacion`) REFERENCES `derivaciones` (`id_derivacion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla kinesiologiadb.antecedentes_derivaciones: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `antecedentes_derivaciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `antecedentes_derivaciones` ENABLE KEYS */;

-- Volcando estructura para tabla kinesiologiadb.derivaciones
CREATE TABLE IF NOT EXISTS `derivaciones` (
  `id_derivacion` int(11) NOT NULL AUTO_INCREMENT,
  `abona` tinyint(4) DEFAULT NULL,
  `diagnostico_medico` blob,
  `diagnostico_kinesico` blob,
  `numero_sesiones` int(11) DEFAULT NULL,
  `motivo_noabona` blob,
  `id_patologia` int(11) NOT NULL,
  `id_medico` int(11) NOT NULL,
  `id_alumno` int(11) NOT NULL,
  `id_ficha` int(11) NOT NULL,
  PRIMARY KEY (`id_derivacion`),
  KEY `fk_Derivacion_Patologia1_idx` (`id_patologia`),
  KEY `fk_Derivacion_Medicos1_idx` (`id_medico`),
  KEY `fk_Derivacion_Alumnos1_idx` (`id_alumno`),
  KEY `fk_Derivaciones_Fichas1_idx` (`id_ficha`),
  CONSTRAINT `fk_Derivacion_Alumnos1` FOREIGN KEY (`id_alumno`) REFERENCES `alumnos` (`id_alumno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Derivacion_Medicos1` FOREIGN KEY (`id_medico`) REFERENCES `medicos` (`id_medico`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Derivacion_Patologia1` FOREIGN KEY (`id_patologia`) REFERENCES `patologias` (`id_patologia`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Derivaciones_Fichas1` FOREIGN KEY (`id_ficha`) REFERENCES `fichas` (`id_ficha`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla kinesiologiadb.derivaciones: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `derivaciones` DISABLE KEYS */;
INSERT INTO `derivaciones` (`id_derivacion`, `abona`, `diagnostico_medico`, `diagnostico_kinesico`, `numero_sesiones`, `motivo_noabona`, `id_patologia`, `id_medico`, `id_alumno`, `id_ficha`) VALUES
	(1, 1, _binary '', _binary '', 12, _binary 0x20, 1, 1, 2, 2);
/*!40000 ALTER TABLE `derivaciones` ENABLE KEYS */;

-- Volcando estructura para tabla kinesiologiadb.estados
CREATE TABLE IF NOT EXISTS `estados` (
  `id_estado` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion_estado` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id_estado`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla kinesiologiadb.estados: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `estados` DISABLE KEYS */;
INSERT INTO `estados` (`id_estado`, `descripcion_estado`) VALUES
	(1, 'Muerto');
/*!40000 ALTER TABLE `estados` ENABLE KEYS */;

-- Volcando estructura para tabla kinesiologiadb.estados_derivaciones
CREATE TABLE IF NOT EXISTS `estados_derivaciones` (
  `id_Derivacion` int(11) NOT NULL,
  `descripcion_estado_derivacion` blob,
  `id_estado` int(11) NOT NULL,
  PRIMARY KEY (`id_Derivacion`,`id_estado`),
  KEY `fk_Estados_has_Derivacion_Derivacion1_idx` (`id_Derivacion`),
  KEY `fk_Estados_de_Derivacion_Estados1_idx` (`id_estado`),
  CONSTRAINT `fk_Estados_de_Derivacion_Estados1` FOREIGN KEY (`id_estado`) REFERENCES `estados` (`id_estado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Estados_has_Derivacion_Derivacion1` FOREIGN KEY (`id_Derivacion`) REFERENCES `derivaciones` (`id_derivacion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla kinesiologiadb.estados_derivaciones: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `estados_derivaciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `estados_derivaciones` ENABLE KEYS */;

-- Volcando estructura para tabla kinesiologiadb.estudios
CREATE TABLE IF NOT EXISTS `estudios` (
  `id_estudio` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion_estudio_complementario` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_estudio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla kinesiologiadb.estudios: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `estudios` DISABLE KEYS */;
/*!40000 ALTER TABLE `estudios` ENABLE KEYS */;

-- Volcando estructura para tabla kinesiologiadb.estudios_derivaciones
CREATE TABLE IF NOT EXISTS `estudios_derivaciones` (
  `fecha_derivacion_estudio` datetime DEFAULT NULL,
  `resultado_derivacion` blob,
  `id_derivacion` int(11) NOT NULL,
  `id_estudio` int(11) NOT NULL,
  PRIMARY KEY (`id_derivacion`,`id_estudio`),
  KEY `fk_Derivacion_Estudios_Complementarios_Derivacion1_idx` (`id_derivacion`),
  KEY `fk_Estudios_Derivaciones_Estudios1_idx` (`id_estudio`),
  CONSTRAINT `fk_Derivacion_Estudios_Complementarios_Derivacion1` FOREIGN KEY (`id_derivacion`) REFERENCES `derivaciones` (`id_derivacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Estudios_Derivaciones_Estudios1` FOREIGN KEY (`id_estudio`) REFERENCES `estudios` (`id_estudio`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla kinesiologiadb.estudios_derivaciones: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `estudios_derivaciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `estudios_derivaciones` ENABLE KEYS */;

-- Volcando estructura para tabla kinesiologiadb.evolucion
CREATE TABLE IF NOT EXISTS `evolucion` (
  `id_evolucion` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_evaluacion` datetime DEFAULT NULL,
  `monto_evaluacion` int(11) DEFAULT NULL,
  `turno` smallint(4) DEFAULT NULL,
  `num_sesion` int(11) DEFAULT NULL,
  `kinesioterapia` blob,
  `fisioterapia` blob,
  `kinefilaxia` blob,
  `id_derivacion` int(11) NOT NULL,
  `id_alumno` int(11) NOT NULL,
  PRIMARY KEY (`id_evolucion`),
  KEY `fk_Evolucion_Derivacion1_idx` (`id_derivacion`),
  KEY `fk_Evolucion_Alumnos1_idx` (`id_alumno`),
  CONSTRAINT `fk_Evolucion_Alumnos1` FOREIGN KEY (`id_alumno`) REFERENCES `alumnos` (`id_alumno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Evolucion_Derivacion1` FOREIGN KEY (`id_derivacion`) REFERENCES `derivaciones` (`id_derivacion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla kinesiologiadb.evolucion: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `evolucion` DISABLE KEYS */;
INSERT INTO `evolucion` (`id_evolucion`, `fecha_evaluacion`, `monto_evaluacion`, `turno`, `num_sesion`, `kinesioterapia`, `fisioterapia`, `kinefilaxia`, `id_derivacion`, `id_alumno`) VALUES
	(1, '2020-11-06 10:27:22', 150000, 1, 1, _binary 0x4D6173616A6573, NULL, NULL, 1, 1),
	(2, '2020-11-09 00:00:00', 160000, 1, 2, _binary 0x416C67756E6120636F7361, NULL, NULL, 1, 1);
/*!40000 ALTER TABLE `evolucion` ENABLE KEYS */;

-- Volcando estructura para tabla kinesiologiadb.fichas
CREATE TABLE IF NOT EXISTS `fichas` (
  `id_ficha` int(11) NOT NULL AUTO_INCREMENT,
  `nro_documento` int(11) DEFAULT NULL,
  `nombre_paciente` varchar(60) NOT NULL,
  `apellido_paciente` varchar(60) NOT NULL,
  `fecha_ingreso` datetime DEFAULT NULL,
  `edad` int(11) DEFAULT NULL,
  `sexo` varchar(45) DEFAULT NULL,
  `fecha_nacimiento` datetime DEFAULT NULL,
  `domicilio_paciente` varchar(60) DEFAULT NULL,
  `telefono_paciente` int(11) DEFAULT NULL,
  `correo_paciente` varchar(60) DEFAULT NULL,
  `id_ocupacion` int(11) NOT NULL,
  PRIMARY KEY (`id_ficha`),
  KEY `fk_Fichas_Ocupaciones1_idx` (`id_ocupacion`),
  CONSTRAINT `fk_Fichas_Ocupaciones1` FOREIGN KEY (`id_ocupacion`) REFERENCES `ocupaciones` (`id_ocupacion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla kinesiologiadb.fichas: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `fichas` DISABLE KEYS */;
INSERT INTO `fichas` (`id_ficha`, `nro_documento`, `nombre_paciente`, `apellido_paciente`, `fecha_ingreso`, `edad`, `sexo`, `fecha_nacimiento`, `domicilio_paciente`, `telefono_paciente`, `correo_paciente`, `id_ocupacion`) VALUES
	(2, 1, 'Fulano', 'Fulanito', '2000-01-01 00:00:00', 1, 'M', '1800-01-01 00:00:00', 'Encarnacion', 123456789, 'yo@tu.el', 1);
/*!40000 ALTER TABLE `fichas` ENABLE KEYS */;

-- Volcando estructura para tabla kinesiologiadb.medicos
CREATE TABLE IF NOT EXISTS `medicos` (
  `id_medico` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_medico` varchar(60) DEFAULT NULL,
  `apellido_medico` varchar(60) DEFAULT NULL,
  `telefono_medico` varchar(60) DEFAULT NULL,
  `correo_medico` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id_medico`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla kinesiologiadb.medicos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `medicos` DISABLE KEYS */;
INSERT INTO `medicos` (`id_medico`, `nombre_medico`, `apellido_medico`, `telefono_medico`, `correo_medico`) VALUES
	(1, 'Carlos', 'Conster', ' ', 'medico@medico.com');
/*!40000 ALTER TABLE `medicos` ENABLE KEYS */;

-- Volcando estructura para tabla kinesiologiadb.objetivos
CREATE TABLE IF NOT EXISTS `objetivos` (
  `id_objetivos` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion_objetivo` blob,
  `id_derivacion` int(11) NOT NULL,
  PRIMARY KEY (`id_objetivos`),
  KEY `fk_Objetivos_Derivacion1_idx` (`id_derivacion`),
  CONSTRAINT `fk_Objetivos_Derivacion1` FOREIGN KEY (`id_derivacion`) REFERENCES `derivaciones` (`id_derivacion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla kinesiologiadb.objetivos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `objetivos` DISABLE KEYS */;
/*!40000 ALTER TABLE `objetivos` ENABLE KEYS */;

-- Volcando estructura para tabla kinesiologiadb.ocupaciones
CREATE TABLE IF NOT EXISTS `ocupaciones` (
  `id_ocupacion` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion_ocupacion` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id_ocupacion`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla kinesiologiadb.ocupaciones: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `ocupaciones` DISABLE KEYS */;
INSERT INTO `ocupaciones` (`id_ocupacion`, `descripcion_ocupacion`) VALUES
	(1, 'Ocupador');
/*!40000 ALTER TABLE `ocupaciones` ENABLE KEYS */;

-- Volcando estructura para tabla kinesiologiadb.patologias
CREATE TABLE IF NOT EXISTS `patologias` (
  `id_patologia` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion_patologia` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_patologia`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla kinesiologiadb.patologias: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `patologias` DISABLE KEYS */;
INSERT INTO `patologias` (`id_patologia`, `descripcion_patologia`) VALUES
	(1, 'Sida');
/*!40000 ALTER TABLE `patologias` ENABLE KEYS */;

-- Volcando estructura para tabla kinesiologiadb.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_usuario` varchar(60) DEFAULT NULL,
  `rol` varchar(60) DEFAULT NULL,
  `clave_usuario` varchar(60) DEFAULT NULL,
  `estado_usuario` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla kinesiologiadb.usuarios: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`id_usuario`, `nombre_usuario`, `rol`, `clave_usuario`, `estado_usuario`) VALUES
	(1, 'admin', 'Administrador', 'admin', 0);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
